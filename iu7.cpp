#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <vector>
#include <functional>
#include "lib/matrix.cpp"

typedef unsigned int ui;
typedef unsigned long long ull;

using namespace std;

// specific problem functions:
const double T = 1.0;
const double X = M_PI;
double f(double x, double t)
{
	return (x+t*t)*exp(-x*t);
}
double g0(double t)
{
	return t-exp(-t);
}
double g1(double t)
{
	return -( exp(-t) + t*exp(-M_PI*t) );
}
double h0(double x)
{
	return 1.0+sin(x);
}


//Problem solving algorithms:

/*
double *ThomasAlgorithm(ui N, double(*A)(ui), double(*B)(ui), double(*C)(ui), double(*F)(ui))
	Takes the size of the matrix N,
	Diagonal elements A(subdiagonal), B(diagonal), C(upperdiagonal) as functions from (ui) rows
	should be defined for unsigned int in range:
		(A) 1..N-1
		(B) 0..N-1
		(C) 0..N-1
*/
double *ThomasAlgorithm(ui N, function<double(ui)> A, function<double(ui)> B, function<double(ui)> C, function<double(ui)> F)
{
	double *unknowns = new double[N], *a = new double[N], *b = new double[N];
	double norm;
	assert(unknowns != NULL);
	assert(a != NULL);
	assert(b != NULL);
	a[0] = -C(0)/B(0);
	b[0] = F(0)/B(0);
	if(abs(a[0]) >= 1.0)
	{
		cout << "Warning! alpha[0] >= 1.0 !\n";
	}
	for(int i = 1; i < N; i++)
	{
		norm = (B(i)+a[i-1]*A(i));
		a[i] = -C(i)/norm;
		b[i] = (F(i)-b[i-1]*A(i))/norm;
	}
	unknowns[N-1] = b[N-1];
	for(int i = N-2; i >= 0; i--)
	{
		unknowns[i] = a[i]*unknowns[i+1]+b[i];
	}
	return unknowns;
}

double CN_A(ui N, ui n, double h, double tau)//Pārbaudīts!
{
	if(n > 0 && n < N)
		return -tau/(2*h*h);
	else if (n == N)
		return -tau/(h*h);
	else
	{
		cout << "CN_A-Warn!\n";
		return 0;
	}
}

double CN_B(ui N, ui n, double h, double tau)//Pārbaudīts!
{
	if(n <= N)
		return (1+tau/(h*h));
	else
	{
		cout << "CN_B-Warn!\n";
		return 0;
	}
}

double CN_C(ui N, ui n, double h, double tau)//
{
	if(n > 0 && n < N)
		return -tau/(2*h*h);
	else if (n == 0)
		return -tau/(h*h);
	else if (n == N)
		return 0;
	else
	{
		cout << "CN_C-Warn!\n";
		return 0;
	}
}

double CN_F(Matrix<double> grid, ui N, ui n, ui j, double h, double tau)//Pārbaudīts!
{
	if(n > 0 && n < N)
		return tau/(2*h*h)*(grid(n+1,j-1)-2*grid(n,j-1)+grid(n-1,j-1)) - tau/2*(f(n*h,(j-1)*tau) + f(n*h,j*tau)) + grid(n,j-1);
	else if (n == 0)
		return tau/(h*h)*grid(1,j-1) + (1-tau/(h*h))*grid(0,j-1) - tau/2*(f(0,(j-1)*tau)+f(0,j*tau)) - tau/h*(g0((j-1)*tau) + g0(j*tau));
	else if (n == N)
		return tau/(h*h)*grid(N-1,j-1) + (1-tau/(h*h))*grid(N,j-1) + tau/h*(g1((j-1)*tau) + g1(j*tau)) - tau/2*(f(N*h,(j-1)*tau)+f(N*h,j*tau));
	else
	{
		cout << "CN_F-Warn!\n";
		return 0;
	}
}

Matrix<double> CrankNicolsonSolver(ui N, ui J)
{
	Matrix<double> grid(N+1, J+1);
	const double tau = T/((double)J), h = X/((double)N);
	double *col;
	for(ui n = 0; n <= N; n++)
		grid.set(n, 0, h0(n*h));
	for(ui j = 1; j <= J; j++)
	{
		col = ThomasAlgorithm(N+1, [&](ui k){ return CN_A(N, k, h, tau); }, [&](ui k){ return CN_B(N, k, h, tau);}, [&](ui k){ return CN_C(N, k, h, tau);}, [&](ui k){ return CN_F(grid, N, k, j, h, tau);} );
		grid.setCol(j, col);
	}
	return grid;
}


double BT_A(ui N, ui n, double h, double tau)
{
	if(n > 0 && n < N)
		return tau/(h*h);
	else if (n == N)
		return 2*tau/(h*h);
	else
	{
		cout << "BT_A-Warn!\n";
		return 0;
	}
}

double BT_B(ui N, ui n, double h, double tau)
{
	if(n <= N)
		return -1.0-2*tau/(h*h);
	else
	{
		cout << "BT_B-Warn!\n";
		return 0;
	}
}

double BT_C(ui N, ui n, double h, double tau)
{
	if(n > 0 && n < N)
		return tau/(h*h);
	else if (n == 0)
		return 2*tau/(h*h);
	else if (n == N)
		return 0;
	else
	{
		cout << "BT_C-Warn!\n";
		return 0;
	}
}

double BT_F(Matrix<double> grid, ui N, ui n, ui j, double h, double tau)
{
	if(n > 0 && n < N)
		return tau*f(n*h, j*tau) - grid(n,j-1);
	else if (n == 0)
		return tau*f(0, j*tau) - grid(0,j-1) + 2*tau/h*g0(j*tau);
	else if (n == N)
		return tau*f(N*h, j*tau) - grid(N,j-1) - 2*tau/h*g1(j*tau);
	else
	{
		cout << "BT_F-Warn!\n";
		return 0;
	}
}

Matrix<double> BackwardTimeSolver(ui N, ui J)
{
	Matrix<double> grid(N+1, J+1);
	const double tau = T/((double)J), h = X/((double)N);
	double *col;
	for(ui n = 0; n <= N; n++)
		grid.set(n, 0, h0(n*h));
	for(ui j = 1; j <= J; j++)
	{
		col = ThomasAlgorithm(N+1, [&](ui k){ return BT_A(N, k, h, tau); }, [&](ui k){ return BT_B(N, k, h, tau);}, [&](ui k){ return BT_C(N, k, h, tau);}, [&](ui k){ return BT_F(grid, N, k, j, h, tau);} );
		grid.setCol(j, col);
	}
	return grid;
}

Matrix<double> ForwardTimeSolver(ui N, ui J)
{
	Matrix<double> grid(N+1, J+1);
	const double tau = T/((double)J), h = X/((double)N);
	for(ui n = 0; n <= N; n++)
		grid.set(n,0,h0(n*h));
	for(ui j = 0; j < J; j++)
	{
		grid.set(0, j+1, 2*tau/(h*h)*grid(1,j) + (1-2*tau/(h*h))*grid(0,j) - 2*tau/h*g0(j*tau) - tau*f(0, j*tau));
		for(ui n = 1; n < N; n++)
			grid.set(n, j+1, tau/(h*h)*grid(n-1,j) + (1-2*tau/(h*h))*grid(n,j) + tau/(h*h)*grid(n+1,j) - tau*f(n*h, j*tau));
		grid.set(N, j+1, (1-2*tau/(h*h))*grid(N,j) + 2*tau/(h*h)*grid(N-1,j) + 2*tau/h*g1(j*tau) - tau*f(N*h, j*tau));
	}
	return grid;
}
//Problem solving algorithms END

// Exporting tools:

void generateFTStabilityResults(string stable, string unstable)
{
	ofstream stableFile(stable);
	//stableFile << "# h		tau		y\n";
	stableFile.precision(5);
	stableFile << std::scientific;
	
	ofstream unstableFile(unstable);
	//unstableFile << "# h		tau		y\n";
	unstableFile.precision(5);
	unstableFile << std::scientific;
	
	double initJ = 2, initN = 4, res;
	double resMax = 0.85, resMin = 0.55;
	int N, J;
	
	for(double j = 1; j >= 0.001; j -= 0.01)
	{
		J = initJ/j;
		for(double n = 1; n >= 0.01; n -= 0.1)
		{
			N = initN/n;
			Matrix<double> grid = ForwardTimeSolver(N, J);
			res = grid(N/2,J);
			if(resMin < res && res < resMax)
				stableFile << M_PI/N << "	" << 1.0/J << "\n";
			else
				unstableFile << M_PI/N << "	" << 1.0/J << "\n";
		}
	}
	stableFile.close();
	unstableFile.close();
}

void plotFinalTime(Matrix<double> grid, double h, string fName)
{
	ofstream of(fName);
	ui J = grid.cols(), N = grid.rows();
	for(ui n = 0; n < N; n++)
	{
		of << n*h << "	" << grid(n, J-1) << "\n";
	}
	of.close();
}

void generateDataAtLastTimeStep(void)
{
	ui tests[] = {32,100,320,1000,3200,10000};
	ui j;
	ui N = 10;
	for(j = 0; j < 5; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, X/N, "FT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, X/N, "BT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotFinalTime(grid, X/N, "CN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
	N = 20;
	for(j = 1; j < 6; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, X/N, "FT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, X/N, "BT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotFinalTime(grid, X/N, "CN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
}

void plotMiddleSpace(Matrix<double> grid, double tau, string fName)
{
	ofstream of(fName);
	ui J = grid.cols(), N = grid.rows();
	for(ui j = 0; j < J; j++)
	{
		of << j*tau << "	" << grid(N/2, j) << "\n";
	}
	of.close();
}

void generateDataAtMiddleSpaceStep(void)
{
	ui tests[] = {32,100,320,1000,3200,10000};
	ui j;
	ui N = 10;
	for(j = 0; j < 5; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotMiddleSpace(grid, T/tests[j], "MidFT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotMiddleSpace(grid, T/tests[j], "MidBT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotMiddleSpace(grid, T/tests[j], "MidCN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
	N = 20;
	for(j = 1; j < 6; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotMiddleSpace(grid, T/tests[j], "MidFT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotMiddleSpace(grid, T/tests[j], "MidBT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotMiddleSpace(grid, T/tests[j], "MidCN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
}


void generateDataForAccuracy(void)
{
	ui iMax = 100, iMin = 25, N = 10, J, initJ = 32;
	ofstream ftf("AccFT-"+to_string(N)+".dat");
	ftf.precision(5);
	ftf << std::scientific;
	ofstream btf("AccBT-"+to_string(N)+".dat");
	btf.precision(5);
	btf << std::scientific;
	ofstream cnf("AccCN-"+to_string(N)+".dat");
	cnf.precision(5);
	cnf << std::scientific;
	for(ui i = iMax; i > iMin; i--)
	{
		J = iMax*initJ/(double)i;
		Matrix<double> grid = ForwardTimeSolver(N, J);
		ftf << T/J << "	" << grid(N/2, J) << "\n";
		grid = BackwardTimeSolver(N, J);
		btf << T/J << "	" << grid(N/2, J) << "\n";
		grid = CrankNicolsonSolver(N, J);
		cnf << T/J << "	" << grid(N/2, J) << "\n";
	}
	ftf.close();
	btf.close();
	cnf.close();
}

void generateCNAcc(void)
{
	ui prev, iMax = 100, iMin = 25, N = 10, J, initJ = 8;
	ofstream cnf("AccCrankNicolson.dat");
	cnf.precision(5);
	cnf << std::scientific;
	for(ui i = iMax; i > iMin; i--)
	{
		J = iMax*initJ/(double)i;
		if(prev == J)
			continue;
		Matrix<double> grid = CrankNicolsonSolver(N, J);
		cnf << 1.0/J << "	" << grid(N/2, J) << "\n";
		prev = J;
	}
	cnf.close();
}


void fprintFor3DPlot(string s)
{
	ofstream of(s);
	of.precision(5);
	of << std::scientific;
	ui J = 300, N = 50;
	double h = X/N, tau = T/J;
	Matrix<double> grid = CrankNicolsonSolver(50, 300);
	for(ui j = 0; j <= J; j++)
	{
		for(ui n = 0; n <= N; n++)
		{
			of << j*tau << "	" << n*h << "	" << grid(n,j) << "\n";
		}
	}
	of.close();
}
int main()
{
	fprintFor3DPlot("3DPlot.dat");
	//generateFTStabilityResults("FTstable.dat", "FTunstable.dat");
	//generateDataAtLastTimeStep();
	//generateDataAtMiddleSpaceStep();
	//generateDataForAccuracy();
	//generateCNAcc();
	return 0;
}
