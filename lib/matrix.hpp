#ifndef MATRIX_H
#define MATRIX_H

using namespace std;

typedef unsigned int ui;

template <class T>
class Matrix
{
public:
	Matrix(void);
	Matrix(ui);
	Matrix(ui, ui);
	Matrix(ui, ui, T);
	~Matrix();
	ui cols(void) const;
	ui rows(void) const;
	T operator()(ui, ui);
	bool set(ui, ui, T);
	bool setCol(ui, T*);
	Matrix<T> operator=(Matrix<T>);
	Matrix copy(void) const;
	void fprint(ofstream&);
	void fprintCol(ofstream&, ui);
	void fprintRow(ofstream&, ui);
	void print(void);
	void printPerm(ui*);
private:
	void init(ui, ui, T);
	ui n;
	ui m;
	T* data;
};

#endif