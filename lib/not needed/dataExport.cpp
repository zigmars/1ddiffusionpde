#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <vector>
#include <functional>
#include "matrix.cpp"
#include "solver.cpp"

typedef unsigned int ui;
typedef unsigned long long ull;

using namespace std;

void generateFTStabilityResults(string stable, string unstable)
{
	ofstream stableFile(stable);
	//stableFile << "# h		tau		y\n";
	stableFile.precision(5);
	stableFile << std::scientific;
	
	ofstream unstableFile(unstable);
	//unstableFile << "# h		tau		y\n";
	unstableFile.precision(5);
	unstableFile << std::scientific;
	
	double initJ = 2, initN = 4, res;
	double resMax = 0.85, resMin = 0.55;
	int N, J;
	
	for(double j = 1; j >= 0.001; j -= 0.01)
	{
		J = initJ/j;
		for(double n = 1; n >= 0.01; n -= 0.1)
		{
			N = initN/n;
			Matrix<double> grid = ForwardTimeSolver(N, J);
			res = grid(N/2,J);
			if(resMin < res && res < resMax)
				stableFile << M_PI/N << "	" << 1.0/J << "\n";
			else
				unstableFile << M_PI/N << "	" << 1.0/J << "\n";
		}
	}
	stableFile.close();
	unstableFile.close();
}

void plotFinalTime(Matrix<double> grid, double h, string fName)
{
	ofstream of(fName);
	ui J = grid.cols(), N = grid.rows();
	for(ui n = 0; n < N; n++)
	{
		of << n*h << "	" << grid(n, J-1) << "\n";
	}
	of.close();
}

void generateDataAtLastTimeStep(void)
{
	ui tests[] = {32,100,320,1000,3200,10000};
	ui j;
	ui N = 10;
	for(j = 0; j < 5; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, M_PI/N, "FT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, M_PI/N, "BT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotFinalTime(grid, M_PI/N, "CN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
	N = 20;
	for(j = 1; j < 6; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, M_PI/N, "FT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotFinalTime(grid, M_PI/N, "BT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotFinalTime(grid, M_PI/N, "CN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
}

void plotMiddleTime(Matrix<double> grid, double tau, string fName)
{
	ofstream of(fName);
	ui J = grid.cols(), N = grid.rows();
	for(ui j = 0; j < J; j++)
	{
		of << j*tau << "	" << grid(N/2, j) << "\n";
	}
	of.close();
}

void generateDataAtMiddleTimeStep(void)
{
	ui tests[] = {32,100,320,1000,3200,10000};
	ui j;
	ui N = 10;
	for(j = 0; j < 5; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotMiddleTime(grid, 1.0/J, "MidFT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotMiddleTime(grid, 1.0/J, "MidBT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotMiddleTime(grid, 1.0/J, "MidCN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
	N = 20;
	for(j = 1; j < 6; j++)
	{
		Matrix<double> grid = ForwardTimeSolver(N, tests[j]);
		plotMiddleTime(grid, 1.0/J, "MidFT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = BackwardTimeSolver(N, tests[j]);
		plotMiddleTime(grid, 1.0/J, "MidBT-"+to_string(N)+"-"+to_string(tests[j])+".dat");
		grid = CrankNicolsonSolver(N, tests[j]);
		plotMiddleTime(grid, 1.0/J, "MidCN-"+to_string(N)+"-"+to_string(tests[j])+".dat");
	}
}