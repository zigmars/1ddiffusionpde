#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <vector>
#include <functional>
#include "lib/matrix.cpp"

typedef unsigned int ui;
typedef unsigned long long ull;

using namespace std;

void test(function<double(int)> func, int k)
{
	cout << func(k) << "\n";
}

int main()
{
	int epsilon = 2;
	test([&](int k){return (double)epsilon+k;}, 13);
	return 0;
}