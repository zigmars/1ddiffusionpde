#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <vector>
#include "lib/matrix.cpp"
#include "lib/solver.cpp"
#include "lib/dataExport.cpp"

typedef unsigned int ui;
typedef unsigned long long ull;

using namespace std;


int main()
{
	generateFTStabilityResults("FTstable.dat", "FTunstable.dat");
	generateDataAtLastTimeStep();
	ofstream outFile("newFT.txt");
	Matrix<double> grid = ForwardTimeSolver(20, 100);
	grid.fprint(outFile);
	outFile.close();
	//grid.print();
	return 0;
}