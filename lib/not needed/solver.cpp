#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <vector>
#include <functional>
#include "matrix.cpp"

typedef unsigned int ui;
typedef unsigned long long ull;

using namespace std;


const double T = 1.0;
const double X = M_PI;
double f(double x, double t)
{
	return (x+t*t)*exp(-x*t);
}
double g0(double t)
{
	return t-exp(-t);
}
double g1(double t)
{
	return -( exp(-t) + t*exp(-M_PI*t) );
}
double h0(double x)
{
	return 1.0+sin(x);
}
//*/


/*
double *ThomasAlgorithm(ui N, double(*A)(ui), double(*B)(ui), double(*C)(ui), double(*F)(ui))
	Takes the size of the matrix N,
	Diagonal elements A(subdiagonal), B(diagonal), C(upperdiagonal) as functions from (ui) rows
	should be defined for unsigned int in range:
		(A) 1..N-1
		(B) 0..N-1
		(C) 0..N-1
*/
double *ThomasAlgorithm(ui N, function<double(ui)> A, function<double(ui)> B, function<double(ui)> C, function<double(ui)> F)
{
	double *unknowns = new double[N], *a = new double[N], *b = new double[N];
	double norm;
	assert(unknowns != NULL);
	assert(a != NULL);
	assert(b != NULL);
	a[0] = -C(0)/B(0);
	b[0] = F(0)/B(0);
	if(abs(a[0]) >= 1.0)
	{
		cout << "Warning! alpha[0] >= 1.0 !\n";
	}
	for(int i = 1; i < N; i++)
	{
		norm = (B(i)+a[i-1]*A(i));
		a[i] = -C(i)/norm;
		b[i] = (F(i)-b[i-1]*A(i))/norm;
	}
	unknowns[N-1] = b[N-1];
	for(int i = N-2; i >= 0; i--)
	{
		unknowns[i] = a[i]*unknowns[i+1]+b[i];
	}
	return unknowns;
}

double CN_A(ui N, ui n, double h, double tau)//Pārbaudīts!
{
	if(n > 0 && n < N)
		return -tau/(2*h*h);
	else if (n == N)
		return -tau/(h*h);
	else
	{
		cout << "CN_A-Warn!\n";
		return 0;
	}
}

double CN_B(ui N, ui n, double h, double tau)//Pārbaudīts!
{
	if(n <= N)
		return (1+tau/(h*h));
	else
	{
		cout << "CN_B-Warn!\n";
		return 0;
	}
}

double CN_C(ui N, ui n, double h, double tau)//
{
	if(n > 0 && n < N)
		return -tau/(2*h*h);
	else if (n == 0)
		return -tau/(h*h);
	else if (n == N)
		return 0;
	else
	{
		cout << "CN_C-Warn!\n";
		return 0;
	}
}

double CN_F(Matrix<double> grid, ui N, ui n, ui j, double h, double tau)//Pārbaudīts!
{
	if(n > 0 && n < N)
		return tau/(2*h*h)*(grid(n+1,j-1)-2*grid(n,j-1)+grid(n-1,j-1)) - tau/2*(f(n*h,(j-1)*tau) + f(n*h,j*tau)) + grid(n,j-1);
	else if (n == 0)
		return tau/(h*h)*grid(1,j-1) + (1-tau/(h*h))*grid(0,j-1) - tau/2*(f(0,(j-1)*tau)+f(0,j*tau)) - tau/h*(g0((j-1)*tau) + g0(j*tau));
	else if (n == N)
		return tau/(h*h)*grid(N-1,j-1) + (1-tau/(h*h))*grid(N,j-1) + tau/h*(g1((j-1)*tau) + g1(j*tau)) - tau/2*(f(N*h,(j-1)*tau)+f(N*h,j*tau));
	else
	{
		cout << "CN_F-Warn!\n";
		return 0;
	}
}

Matrix<double> CrankNicolsonSolver(ui N, ui J)
{
	Matrix<double> grid(N+1, J+1);
	const double tau = T/((double)J), h = X/((double)N);
	double *col;
	for(ui n = 0; n <= N; n++)
		grid.set(n, 0, h0(n*h));
	for(ui j = 1; j <= J; j++)
	{
		col = ThomasAlgorithm(N+1, [&](ui k){ return CN_A(N, k, h, tau); }, [&](ui k){ return CN_B(N, k, h, tau);}, [&](ui k){ return CN_C(N, k, h, tau);}, [&](ui k){ return CN_F(grid, N, k, j, h, tau);} );
		grid.setCol(j, col);
	}
	return grid;
}


double BT_A(ui N, ui n, double h, double tau)
{
	if(n > 0 && n < N)
		return tau/(h*h);
	else if (n == N)
		return 2*tau/(h*h);
	else
	{
		cout << "BT_A-Warn!\n";
		return 0;
	}
}

double BT_B(ui N, ui n, double h, double tau)
{
	if(n <= N)
		return -1.0-2*tau/(h*h);
	else
	{
		cout << "BT_B-Warn!\n";
		return 0;
	}
}

double BT_C(ui N, ui n, double h, double tau)
{
	if(n > 0 && n < N)
		return tau/(h*h);
	else if (n == 0)
		return 2*tau/(h*h);
	else if (n == N)
		return 0;
	else
	{
		cout << "BT_C-Warn!\n";
		return 0;
	}
}

double BT_F(Matrix<double> grid, ui N, ui n, ui j, double h, double tau)
{
	if(n > 0 && n < N)
		return tau*f(n*h, j*tau) - grid(n,j-1);
	else if (n == 0)
		return tau*f(0, j*tau) - grid(0,j-1) + 2*tau/h*g0(j*tau);
	else if (n == N)
		return tau*f(N*h, j*tau) - grid(N,j-1) - 2*tau/h*g1(j*tau);
	else
	{
		cout << "BT_F-Warn!\n";
		return 0;
	}
}

Matrix<double> BackwardTimeSolver(ui N, ui J)
{
	Matrix<double> grid(N+1, J+1);
	const double tau = T/((double)J), h = X/((double)N);
	double *col;
	for(ui n = 0; n <= N; n++)
		grid.set(n, 0, h0(n*h));
	for(ui j = 1; j <= J; j++)
	{
		col = ThomasAlgorithm(N+1, [&](ui k){ return BT_A(N, k, h, tau); }, [&](ui k){ return BT_B(N, k, h, tau);}, [&](ui k){ return BT_C(N, k, h, tau);}, [&](ui k){ return BT_F(grid, N, k, j, h, tau);} );
		grid.setCol(j, col);
	}
	return grid;
}

Matrix<double> ForwardTimeSolver(ui N, ui J)
{
	Matrix<double> grid(N+1, J+1);
	const double tau = T/((double)J), h = X/((double)N);
	for(ui n = 0; n <= N; n++)
		grid.set(n,0,h0(n*h));
	for(ui j = 0; j < J; j++)
	{
		grid.set(0, j+1, 2*tau/(h*h)*grid(1,j) + (1-2*tau/(h*h))*grid(0,j) - 2*tau/h*g0(j*tau) - tau*f(0, j*tau));
		for(ui n = 1; n < N; n++)
			grid.set(n, j+1, tau/(h*h)*grid(n-1,j) + (1-2*tau/(h*h))*grid(n,j) + tau/(h*h)*grid(n+1,j) - tau*f(n*h, j*tau));
		grid.set(N, j+1, (1-2*tau/(h*h))*grid(N,j) + 2*tau/(h*h)*grid(N-1,j) + 2*tau/h*g1(j*tau) - tau*f(N*h, j*tau));
	}
	return grid;
}