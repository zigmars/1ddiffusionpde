#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>
#include "matrix.hpp"

using namespace std;

typedef unsigned int ui;

template <class T>
Matrix<T>::Matrix(void)
{
	data = NULL;
	n = m = 0;
}

template <class T>
Matrix<T>::Matrix(ui n_)
{
	init(n_, n_, (T)0);
}

template <class T>
Matrix<T>::Matrix(ui n_, ui m_)
{
	init(n_, m_, (T)0);
}

template <class T>
Matrix<T>::Matrix(ui n_, ui m_, T val)
{
	init(n_, m_, val);
}

template <class T>
void Matrix<T>::init(ui n_, ui m_, T val)
{
	n = n_;
	m = m_;
	data = new T[n*m];
	if(data == NULL)
	{
		cout << "\nCould not allocate matrix memory\n";
	}
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			data[i * m + j] = val;
		}
	}
}

template <class T>
Matrix<T>::~Matrix()
{
	if(0)//data != NULL)//kā parasti uz os x kkas fokin nestrādā ar manuālo atmiņas atbrīvošanu
	{
		delete [] data;
		data = NULL;
	}
}

template <class T>
ui Matrix<T>::rows(void) const
{
	return n;
}

template <class T>
ui Matrix<T>::cols(void) const
{
	return m;
}

template <class T>
T Matrix<T>::operator()(ui i, ui j)
{
	// cout << "i = " << i << ", j = " << j << "\n";
	assert(i < this->n && j < this->m);
	return data[i*m + j];
}

template <class T>
bool Matrix<T>::set(ui i, ui j, T val)
{
	if(i >= n || j >= m)
		return false;
	data[i*m + j] = val;
	return true;
}

template <class T>
bool Matrix<T>::setCol(ui c, T *col)
{
	if(c >= m)
		return false;
	for(ui i = 0; i < n; i++)
	{
		data[i*m+c] = col[i];
	}
	return true;
}

template <class T>
Matrix<T> Matrix<T>::operator=(Matrix<T> rhs)
{
	n = rhs.rows();
	m = rhs.cols();
	for(ui i = 0; i < n; i++)
	{
		for(ui j = 0; j < m; j++)
		{
			set(i,j,rhs(i,j));
		}
	}
	return *this;
}

template <class T>
Matrix<T> Matrix<T>::copy(void) const
{
	Matrix<T> *tmp = new Matrix(n,m);
	*tmp = *this;
	return *tmp;
}

template <class T>
void Matrix<T>::fprint(ofstream &out)
{
	out.precision(15);
	out << std::scientific;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			out << data[i * m + j] << " 	 ";
		}
		out << "\n";
	}
}

template <class T>
void Matrix<T>::fprintCol(ofstream &out, ui k)
{
	out.precision(15);
	out << std::scientific;
	for(int i = 0; i < n; i++)
	{
		out << data[i * m + k] << " 	 ";
	}
	out << "\n";
}

template <class T>
void Matrix<T>::fprintRow(ofstream &out, ui k)
{
	out.precision(15);
	out << std::scientific;
	for(int i = 0; i < m; i++)
	{
		out << data[k * m + i] << " 	 ";
	}
	out << "\n";
}

template <class T>
void Matrix<T>::print()
{
	cout.precision(5);
	cout << std::scientific;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			cout << data[i * m + j] << " 	 ";
		}
		cout << "\n";
	}
}

template <class T>
void Matrix<T>::printPerm(ui *P)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			cout << data[P[i] * m + j] << " 	 ";
		}
		cout << "\n";
	}
}