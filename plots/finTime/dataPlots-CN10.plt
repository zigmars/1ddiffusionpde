set title "Skaitliskā atrisinājuma vērtības laika slānī t = T = 1"
set term svg size 1000 700
set termoption dash
set output "dataCN10.svg"
set key box opaque
set key invert
set key top right
set grid
set xlabel "Telpas koordināta x"
set ylabel "Funkcijas vērtība u(x,T=1)"
set yrange [0:1]

set style line 11 lt 1 linecolor rgb "red" lw 2 pt 1
set style line 12 lt 2 linecolor rgb "red" lw 2 pt 2
set style line 13 lt 3 linecolor rgb "red" lw 2 pt 3
set style line 14 lt 4 linecolor rgb "red" lw 2 pt 4
set style line 15 lt 5 linecolor rgb "red" lw 2 pt 5
set style line 16 lt 6 linecolor rgb "red" lw 2 pt 6

set style line 21 lt 1 linecolor rgb "green" lw 2 pt 1
set style line 22 lt 2 linecolor rgb "green" lw 2 pt 2
set style line 23 lt 3 linecolor rgb "green" lw 2 pt 3
set style line 24 lt 4 linecolor rgb "green" lw 2 pt 4
set style line 25 lt 5 linecolor rgb "green" lw 2 pt 5
set style line 26 lt 6 linecolor rgb "green" lw 2 pt 6

set style line 31 lt 1 linecolor rgb "blue" lw 2 pt 1
set style line 32 lt 2 linecolor rgb "blue" lw 2 pt 2
set style line 33 lt 3 linecolor rgb "blue" lw 2 pt 3
set style line 34 lt 4 linecolor rgb "blue" lw 2 pt 4
set style line 35 lt 5 linecolor rgb "blue" lw 2 pt 5
set style line 36 lt 6 linecolor rgb "blue" lw 2 pt 6

plot "data/CN-10-32.dat" title "Kranka-Nikolsona shēma h=pi/10, tau=1/32" ls 22, \
	"data/CN-10-100.dat" title "Kranka-Nikolsona shēma h=pi/10, tau=1/100" ls 11, \
	"data/CN-10-3200.dat" title "Kranka-Nikolsona shēma h=pi/10, tau=1/3200" ls 33
