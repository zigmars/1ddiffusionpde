set title "Skaitliskā atrisinājuma vērtības vidējā telpas slānī x=pi/2"
set term svg size 1000 700
set termoption dash
set output "dataMidBT20.svg"
set key box opaque
set key invert
set key top right
set grid
set xlabel "Laika koordināta t"
set ylabel "Funkcijas vērtība u(pi/2,t)"
set yrange [0:2]

set style line 11 lt 1 linecolor rgb "red" lw 2 pt 1 ps 0.7
set style line 12 lt 2 linecolor rgb "red" lw 2 pt 2
set style line 13 lt 3 linecolor rgb "red" lw 2 pt 3
set style line 14 lt 4 linecolor rgb "red" lw 2 pt 4
set style line 15 lt 5 linecolor rgb "red" lw 2 pt 5
set style line 16 lt 6 linecolor rgb "red" lw 2 pt 6

set style line 21 lt 1 linecolor rgb "green" lw 2 pt 1
set style line 22 lt 2 linecolor rgb "green" lw 2 pt 2
set style line 23 lt 3 linecolor rgb "green" lw 2 pt 3
set style line 24 lt 4 linecolor rgb "green" lw 2 pt 4
set style line 25 lt 5 linecolor rgb "green" lw 2 pt 5
set style line 26 lt 6 linecolor rgb "green" lw 2 pt 6

set style line 31 lt 1 linecolor rgb "blue" lw 2 pt 1
set style line 32 lt 2 linecolor rgb "blue" lw 2 pt 2
set style line 33 lt 3 linecolor rgb "blue" lw 1 pt 3 ps 0.2
set style line 34 lt 4 linecolor rgb "blue" lw 2 pt 4
set style line 35 lt 5 linecolor rgb "blue" lw 2 pt 5
set style line 36 lt 6 linecolor rgb "blue" lw 2 pt 6

plot "data/MidBT-20-100.dat" title "Slēptā shēma h=pi/20, tau=1/100" ls 22, \
	"data/MidBT-20-320.dat" title "Slēptā shēma h=pi/20, tau=1/320" ls 11, \
	"data/MidBT-20-10000.dat" title "Slēptā shēma h=pi/20, tau=1/10000" ls 33
