set title "Shēmu vērtības u(pi/2,T=1) atkarībā no laika soļa platuma\nh=pi/10"
set term svg size 1000 700
set termoption dash 
set output "accuracy.svg"
set key box opaque
set key invert
set key top left
set xlabel "Laika solis tau"
set ylabel "Funkcijas vērtība u(pi/2, 1)"
#set yrange [0:0.4]

f1(x) = a1+b1*x**c1
f2(x) = a2+b2*x**c2
f3(x) = a3+b3*x**c3

fit f1(x) "AccFT-10.dat" via a1, b1, c1
fit f2(x) "AccBT-10.dat" via a2, b2, c2
fit f3(x) "AccCN-10.dat" via a3, b3, c3


set label sprintf("Piedzītā formula:\n psi(tau) = %.5f + %.3f * tau^%.3f", a1, b1, c1) at 0.023, 0.728
set label sprintf("Piedzītā formula:\n psi(tau) = %.5f + %.3f * tau^%.3f", a2, b2, c2) at 0.01, 0.711
set label sprintf("Piedzītā formula:\n psi(tau) = %.5f + %.3f * tau^%.3f", a3, b3, c3) at 0.025, 0.721

plot "AccFT-10.dat" title "Atklātā shēma h=pi/10", \
	f1(x) title "Atklātās shēma", \
	"AccBT-10.dat" title "Slēptā shēma h=pi/10", \
	f2(x) title "Slēptās shēma", \
	"AccCN-10.dat" title "Kranka-Nikolsona shēma h=pi/10", \
	f3(x) title "Kranka-Nikolsona shēma"
