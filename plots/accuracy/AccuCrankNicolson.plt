set title "Kranka-Nikolsona shēmas aprēķinātās vērtības u(pi/2,T=1) atkarībā no laika soļa platuma\nh=pi/10"
set term svg size 1000 700
set termoption dash 
set output "AccCN.svg"
set key box opaque
set key invert
set key top left
set xlabel "Laika solis tau"
set ylabel "Funkcijas vērtība u(pi/2, 1)"
set yrange [:0.720]

f(x) = a+b*x**c

fit f(x) "AccCrankNicolson.dat" via a, b, c

set label sprintf("Piedzītā formula:\n psi(tau) = %.5f + %.3f * tau^%.3f", a, b, c) at 0.07, 0.716

plot "AccCrankNicolson.dat" title "Kranka-Nikolsona shēma h=pi/10", \
	f(x) title "Kranka-Nikolsona shēma"
