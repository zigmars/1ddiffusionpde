set title "Stabilitātes sadalījums atklātai shēmai\n(ņemts stabils, ja noteiktā y vērtība y(pi/2 ; 1) pieder [0.55 ; 0.85])"
set term svg size 1000 700
set output "stability.svg"
set key box opaque
set key invert
set key top left
set xlabel "Telpas solis h"
set ylabel "Laika solis tau"
set yrange [0:0.4]

plot "FTstable.dat" title "Stabilie dati", \
	"FTunstable.dat" title "Nestabilie dati", \
	x**2/2 title "Teorētiskais nosacījums tau <= (1/2)*h^2"
